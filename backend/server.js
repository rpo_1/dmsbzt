let express = require("express"),
  path = require("path"),
  cors = require("cors"),
  bodyParser = require("body-parser");

const app = express();
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: false
  })
);
app.use(cors());
console.log(path.join(__dirname, "../frontend/dist/frontend"));
app.use(express.static(path.join(__dirname, "../frontend/dist/frontend")));
app.use("/", express.static(path.join(__dirname, "../frontend/dist/frontend")));

// Create port
const port = process.env.PORT || 4000;
const server = app.listen(port, () => {
  console.log("Connected to port " + port);
});
