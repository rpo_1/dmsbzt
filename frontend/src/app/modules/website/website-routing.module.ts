import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from './pages/homepage/homepage.component';
import { Homepage2Component } from './pages/homepage2/homepage2.component';


const routes: Routes = [
  // { path: '', redirectTo: 'domov', pathMatch: 'full' },
  { path: '', component: Homepage2Component },
  // { path: 'admin', loadChildren: './modules/admin/admin.module#AdminModule' },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WebsiteRoutingModule { }
