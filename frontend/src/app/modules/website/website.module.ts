import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomepageComponent } from './pages/homepage/homepage.component';
import { WebsiteRoutingModule } from './website-routing.module';
import { Homepage2Component } from './pages/homepage2/homepage2.component';

@NgModule({
  declarations: [HomepageComponent, Homepage2Component],
  imports: [CommonModule, WebsiteRoutingModule]
})
export class WebsiteModule {}
