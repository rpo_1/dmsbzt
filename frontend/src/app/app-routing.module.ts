import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WebsiteModule } from './modules/website/website.module';

const routes: Routes = [
  // { path: '', redirectTo: 'domov', pathMatch: 'full' },
  { path: '', loadChildren: () => WebsiteModule },
  // { path: 'admin', loadChildren: './modules/admin/admin.module#AdminModule' },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
